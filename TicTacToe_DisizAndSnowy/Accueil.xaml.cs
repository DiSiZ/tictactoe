﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace TicTacToe_DisizAndSnowy
{
    public partial class Accueil : Page
    {
        #region Public
        public static string j1;
        public static string j2;
        #endregion

        #region Constructeur
        public Accueil()
        {
            InitializeComponent();
        }
        #endregion

        #region Chargement Page
        private void NewGameB_Click(object sender, RoutedEventArgs e)
        {
            j1 = P1TB.Text;
            j2 = P2TB.Text;
            if (j1 != "" && j2 != "")
                NavigationService.Navigate(new Uri(@"GamePage.xaml", UriKind.Relative));
        }

        private void StatsB_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"ClassementPage.xaml", UriKind.Relative));
        }
        #endregion
    }
}
