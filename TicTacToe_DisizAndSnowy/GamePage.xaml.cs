﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Media;

namespace TicTacToe_DisizAndSnowy
{
    /// <summary>
    /// Logique d'interaction pour GamePage.xaml
    /// </summary>
    public partial class GamePage : Page, INotifyPropertyChanged
    {
        #region Private
        private GameView gameView;
        private bool gameOver = false;
        #endregion

        #region Initialisation Partie
        public GamePage()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            gameView = new GameView();
            ContainerGame.Children.Cast<Button>().ToList().ForEach(button =>
            {
                button.Content = string.Empty;
                button.Foreground = Brushes.Orange;
                button.IsEnabled = true;
            });
            DataContext = gameView;
        }
        #endregion

        #region Gestion Partie
        private void Button_Click(object sender, System.Windows.RoutedEventArgs ev)
        {
            //-- Evenement click
            gameOver = gameView.OnClick(sender, ev); 

            //-- Si la partie est terminé
            if (gameOver)
            {
                ContainerGame.Children.Cast<Button>().ToList().ForEach(button =>
                {
                    button.IsEnabled = false;
                });
                Restart.IsEnabled = true;
            }
        }

        private void Restart_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Restart.IsEnabled = false;
            Init();
        }
        #endregion

        #region Retour
        private void Back_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"Accueil.xaml", UriKind.Relative));        
        }
        #endregion

        #region Property Changed
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
