﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TicTacToe_DisizAndSnowy
{
    /// <summary>
    /// Logique d'interaction pour ClassementPage.xaml
    /// </summary>
    public partial class ClassementPage : Page
    {
        #region Constructeur
        public ClassementPage()
        {
            InitializeComponent();
            PrintStats();
        }
        #endregion

        #region Interface Classement
        private void PrintStats()
        {
            ListViewPeople.ItemsSource = Util_CSV.ReadCSV();
        }


        private void Back_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"Accueil.xaml", UriKind.Relative));
        }


        private void Reinitialize_CSV_Click(object sender, RoutedEventArgs e)
        {
            Util_CSV.EmptyCSV();
            ListViewPeople.ItemsSource = null;
        }
        #endregion
    }
}
